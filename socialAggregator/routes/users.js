var express = require('express');
var router = express.Router();
var facebook = require('../services/facebook')('1690002187956797','d419cb4ba62343efb549e08ebd43e7e5')
var twitter = require('../services/twitter')('ZqlU9dSEtgwJORxY8f2BWjJfX','erz3tXnFkjMPouRHuxzzadPY5gR5mMGiFAvINYVrKWN5EuJrpd')

router.use('/', (req, res, next) => {

    if(!req.user){
        res.redirect('/');
    }
    next();
});

router.use('/', (req, res, next) => {
    if(req.user.twitter){
        twitter.getUserTimeLine(req.user.twitter.token,
                                req.user.twitter.tokenSecret,
                                req.user.twitter.id,
                                function(results){
                                    req.user.twitter.lastPost = results[0].text;
                                    next();
                                })
    }
});

/* GET users listing. */
router.get('/', function(req, res, next) {
  // console.log('users', req.user);
  if(req.user.facebook){
      facebook.getImage(req.user.facebook.token, function(results){
          req.user.facebook.image = results.url;
          facebook.getFriends(req.user.facebook.token, function(results){
              req.user.facebook.friends = results.total_count;
              res.render('users', {user: req.user});
          })
      });
  }
  else {
      res.render('users', {user: req.user});
  }
});

module.exports = router;
